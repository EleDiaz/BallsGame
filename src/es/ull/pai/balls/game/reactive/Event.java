/**
 * BallsGame - Event.java 7/05/16
 *
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.reactive;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Abstracts the process of generate events and its combinators. Its a value that varies in discrete time.
 */
public class Event<V> {
    // Listener of value
    private ArrayList<Consumer<V>> listeners = new ArrayList<>();
    // Filter function
    private Predicate<V> filter = v -> true;

    /**
     * Send a value, trigger of generation of a event
     * @param value
     */
    public Event<V> send(V value) {
        if (getFilter().test(value)) {
            for (Consumer<V> consumer : getListeners()) {
                consumer.accept(value);
            }
        }
        return this;
    }

    /**
     * Add a listener to be call when a event happens
     * @param consumer
     */
    public Event<V> onDispatch(Consumer<V> consumer) {
        listeners.add(consumer);
        return this;
    }

    /**
     * Join a pair of events into a new one.
     * @param event
     * @return the new event with both sources join
     */
    public Event<V> merge(Event<V> event) {
        Event<V> ev = new Event<>();
        onDispatch(ev::send); // relaunch event to new event
        event.onDispatch(ev::send);
        return ev;
    }

    /**
     * Filter events depends of its values. Its returns a new event with filter set.
     * If predicate is true is let launch event in otherwise no launch event.
     * @param selectFunc function used to filter events
     * @return new event with applied property
     */
    public Event<V> filter(Predicate<V> selectFunc) {
        Event<V> ev = newEventLink();
        ev.setFilter(selectFunc);
        return ev;
    }

    /**
     * Apply a transformation to event
     * @param mapper transformation
     * @param <B>  output of transformation
     * @return new event with applied transformation
     */
    public <B> Event<B> map(Function<V, B> mapper) {
        Event<B> ev = new Event<>();
        onDispatch(v -> ev.send(mapper.apply(v)));
        return ev;
    }

    /**
     * Generate a event with samples of behavior when "this" event is dispatch
     * @param behavior
     * @param <B>
     * @return
     */
    public <B> Event<B> snapshot(Behavior<B> behavior) {
        Event<B> ev = new Event<>();
        onDispatch(v -> ev.send(behavior.getSample()));
        return ev;
    }

    public Behavior<V> hold(V initial) {
        return new Behavior<V>(initial, this);
    }

    /**
     * Helper function to link new event with source sink of before
     * @return
     */
    private Event<V> newEventLink() {
        Event<V> ev = new Event<>();
        onDispatch(ev::send);
        return ev;
    }

    private ArrayList<Consumer<V>> getListeners() {
        return listeners;
    }

    private void setListeners(ArrayList<Consumer<V>> listeners) {
        this.listeners = listeners;
    }

    private Predicate<V> getFilter() {
        return filter;
    }

    private void setFilter(Predicate<V> filter) {
        this.filter = filter;
    }
}
