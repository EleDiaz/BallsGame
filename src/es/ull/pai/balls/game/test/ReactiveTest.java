/**
 * BallsGame - Reactive.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.test;

import static org.junit.Assert.*;

import com.pholser.junit.quickcheck.generator.java.lang.IntegerGenerator;
import es.ull.pai.balls.game.reactive.Behavior;
import es.ull.pai.balls.game.reactive.Event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * @author eleazardd
 */


@RunWith(JUnitQuickcheck.class)
public class ReactiveTest {

    @Property
    public void dispatchListener(Integer value) {
        Event<Integer> ev = new Event<>();
        ev.onDispatch(i -> assertEquals(i, value));
        ev.send(value);
    }

    @Property
    public void dispatchChainListener(Integer value) {
        Event<Integer> ev = new Event<>();
        Event<Integer> ev2 = new Event<>();
        ev.onDispatch(ev2::send);
        ev.send(value);
        ev2.onDispatch(i -> assertEquals(i, value));
    }

    @Property
    public void mergeEvents(Integer value, Integer value2) {
        ArrayList<Integer> get = new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        result.add(value);
        result.add(value2);

        Event<Integer> ev = new Event<>();
        Event<Integer> ev2 = new Event<>();
        ev.merge(ev2).onDispatch(get::add);

        ev.send(value);
        ev.send(value2);

        assertEquals(get, result);
    }

    @Property
    public void mapEvent(Integer value) {
        Event<Integer> ev = new Event<>();
        ev.map(i -> -i).onDispatch(i -> assertEquals(i, (Integer) (-value)));
        ev.send(value);
    }

    @Property
    public void filterEvents(ArrayList<Integer> values) {
        Predicate<Integer> odd = v -> v % 2 == 0;

        ArrayList<Integer> result = (ArrayList<Integer>) values.clone();
        ArrayList<Integer> get = new ArrayList<>();

        result.removeIf(v -> !odd.test(v));
        Event<Integer> ev = new Event<Integer>().filter(odd);
        ev.onDispatch(get::add);
        for (Integer i : values) {
            ev.send(i);
        }
        assertEquals(result, get);
    }
}