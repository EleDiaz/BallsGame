/**
 * BallsGame - Cannon.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.view;

import es.ull.pai.balls.game.reactive.Behavior;
import es.ull.pai.balls.game.utils.Tuple;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;


/**
 * Draw and represents a cannon. (Arrow)
 */
public class Cannon extends DrawerComponent {
    private final Double ARROW_HEAD_SIZE = 1.0 / 5;                 // Head size of cannon
    private final Double THICKNESS = 1.0 / 30;                      // Thickness of cannon
    private final double ARROW_SIZE = 100;                          // Large of cannon
    private Tuple<Double> pos = new Tuple<>(0.0, 0.0);              // Pos to draw
    private Behavior<Double> angle;                                 // Angle of cannon
    private Behavior<Color> color = new Behavior<Color>(Color.BLACK);   // Color of lines to draw cannon

    /**
     * Initialize cannon
     * @param angle Set angle in which target
     */
    public Cannon(Double angle) {
        this.angle = new Behavior<Double>(angle);
    }

    /**
     * Draw Cannon with attributes in a given draw context
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D) {
        // REMEMBER ALWAYS DRAW FROM ORIGIN!
        AffineTransform mat = graphics2D.getTransform();
        Path2D arrow = new Path2D.Double();
        arrow.moveTo(0, 0);
        arrow.lineTo(0, SIZE.getY());
        arrow.moveTo(0, SIZE.getY());
        arrow.lineTo(-(SIZE_MIDDLE.getX() * ARROW_HEAD_SIZE / 2), SIZE.getY() - (SIZE.getY() * ARROW_HEAD_SIZE));
        arrow.moveTo(0, SIZE.getY());
        arrow.lineTo(SIZE_MIDDLE.getX() * ARROW_HEAD_SIZE / 2, SIZE.getY() - (SIZE.getY() * ARROW_HEAD_SIZE));
        graphics2D.translate(pos.getX(), pos.getY());
        graphics2D.scale(ARROW_SIZE, ARROW_SIZE);
        graphics2D.rotate(angle.getSample() - Math.PI / 2);  // its in vertical
        graphics2D.setColor(color.getSample());
        graphics2D.setStroke(new BasicStroke(THICKNESS.floatValue()));
        graphics2D.draw(arrow);
        graphics2D.setTransform(mat);
    }

    /**
     * Get Color
     */
    public Behavior<Color> getColor() {
        return color;
    }

    /**
     * Set color
     * @param color
     * @return
     */
    public Cannon setColor(Behavior<Color> color) {
        this.color = color;
        return this;
    }

    /**
     * Get Angle
     */
    public Behavior<Double> getAngle() {
        return angle;
    }

    /**
     * Get Angle
     * @param angle
     * @return
     */
    public Cannon setAngle(Behavior<Double> angle) {
        this.angle = angle;
        return this;
    }

    /**
     * Get Pos
     */
    public Tuple<Double> getPos() {
        return pos;
    }

    /**
     * Set Pos
     * @param pos
     * @return
     */
    public Cannon setPos(Tuple<Double> pos) {
        this.pos = pos;
        return this;
    }
}