/**
 * BallsGame - DrawerComponent.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.view;

import es.ull.pai.balls.game.utils.Tuple;

import java.awt.*;

/**
 * By default all components that there are "drawer". This drawer are used to draw over determinate canvas(Graphics)
 * from a JPanel.
 */
abstract class DrawerComponent {
    // Helper Constants
    protected final Tuple<Double> SIZE = new Tuple<>(1.0, 1.0);
    protected final Tuple<Double> SIZE_MIDDLE = new Tuple<>(SIZE.getX() / 2, SIZE.getY() / 2);

    /**
     * How is draw the shape is defined here override it
     * @param graphics2D
     */
    abstract void draw(Graphics2D graphics2D);
}
