/**
 * BallsGame - Balls.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.view;

import es.ull.pai.balls.game.model.BallModel;
import es.ull.pai.balls.game.utils.Tuple;

import java.awt.*;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Draw A set of Balls
 */
public class Balls extends Ball {
    private ArrayList<Optional<Ball>> balls = new ArrayList<>();
    private Tuple<Double> pos = new Tuple<>(0.0, 0.0);

    /**
     * Set a number of balls to create
     * @param nBalls
     */
    public Balls(int nBalls) {
        setNumBalls(nBalls);
    }


    public Balls(double width) {
         setNumBalls((int) (width / getDiameter()));
    }

    public void setNumBalls(int nBalls) {
        getBalls().clear();
        for (int i = 0; i < nBalls; i++) {
            getBalls().add(Optional.of(new Ball()));
        }

    }

    @Override
    public void draw(Graphics2D graphics2D) {
        for (int i = 0; i < getBalls().size(); i++) {
            if (getBalls().get(i).isPresent()) {
                getBalls().get(i).get()
                        .setPos(new Tuple<>(getPos().getX() + getDiameter() * i + getDiameter() / 2
                                           , getPos().getY() + getDiameter() / 2))
                        .draw(graphics2D);
            }
        }
    }

    public ArrayList<BallModel> getModels() {
        ArrayList<BallModel> result = new ArrayList<>();
        for (Optional<Ball> ball : getBalls()) {
            ball.ifPresent(ball1 ->
                result.add(ball1.getModel())
            );
        }
        return result;
    }

    /**
     * Remove a ball from set of balls in screen
     * @param ball
     */
    public void removeBall(BallModel ball) {
        double x = ball.getPos().getX();
        double y = ball.getPos().getY();
        for (int i = 0; i < getBalls().size(); i++) {
            if (getBalls().get(i).isPresent()) {
                double xx = getBalls().get(i).get().getPos().getX();
                double yy = getBalls().get(i).get().getPos().getY();
                if (xx == x && y == yy) {
                    getBalls().set(i, Optional.empty());
                }
            }
        }
    }

    /**
     * Get Balls
     */
    public ArrayList<Optional<Ball>> getBalls() {
        return balls;
    }

    /**
     * Set Balls
     * @param balls
     * @return
     */
    public Balls setBalls(ArrayList<Optional<Ball>> balls) {
        this.balls = balls;
        return this;
    }

    /**
     *
     */
    @Override
    public Tuple<Double> getPos() {
        return pos;
    }

    @Override
    public Balls setPos(Tuple<Double> pos) {
        this.pos = pos;
        return this;
    }
}
