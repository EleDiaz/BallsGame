/**
 * BallsGame - Resources.java 8/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game;

import javax.swing.*;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Take from https://github.com/AdrianBZG/Design_Patterns_Example_Codes/blob/master/Codes/Singleton/SingletonClass.java
 * and edited by Eleazar
 */
public class Resources {
    private static Resources instance;
    private ImageIcon infoIcon;
    private AudioClip blink;

    /**
     * Useless constructor
     */
    private Resources() {
        infoIcon = new ImageIcon("./Resources/Info.png");
        blink    = Applet.newAudioClip(getClass().getResource("/es/ull/pai/balls/game/blink.wav"));
    }

    /**
     * This is the important method, that allows clients to access the instance
     * @return the instance
     */
    public static Resources getInstance() {
        if (instance == null) {
            instance = new Resources();
        }
        return instance;
    }

    /**
     * Getter for the info image attribute
     * @return the value
     */
    public ImageIcon getInfoIcon() {
        return infoIcon;
    }

    /**
     * Getter for blink sound
     */
    public AudioClip getBlink() {
        return blink;
    }
}
