/**
 * BallsGame - BallCollision.java 10/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.balls.game.model;


import java.util.ArrayList;

/**
 * Type data returned when a collision between balls happens
 * WARNING: always doubt have two balls in a collision
 */
public class BallCollision extends Collision {
    private BallModel mainBall;                   // Main ball launch(in move)
    private ArrayList<BallModel> ballCollisioned; // collisioned balls with main ball

    // Real type: BallModel -> { balls:[BallModel] | balls.size() > 1 } -> BallCollision
    public BallCollision(BallModel ball, ArrayList<BallModel> balls) {
        mainBall = ball;
        ballCollisioned = balls;
    }

    public boolean isDoubleImpact() {
        return ballCollisioned.size() > 2;
    }

    public BallModel getBallImpact() {
        return ballCollisioned.get(0);
    }
}
