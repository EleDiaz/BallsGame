/**
 * BallsGame - BallsGame.java May 1, 2016
 * 
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */
package es.ull.pai.balls.game;

import es.ull.pai.balls.game.view.AppView;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.JFrame;

/**
 * Disparar bolas de diferentes colores con un cañon
 * - Al impactar debe reproducir un sonido y eliminar la bola
 * - Si impacta "con dos" se considera fallido
 * - no hacer nada mas
 */


/**
 * Defines main that could be a applet or a standlone app
 * 
 * @author eleazardd
 */
public class BallsGame extends JApplet {
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "Balls Game"; // Title app

    /** Initialize the applet */
    public void init() {

        setLayout(new BorderLayout());
        add(new AppView());
    }

    /**
     * Main method to show app in standalone
     * 
     */
    public static void main(String[] args) {
        // Create a frame
        JFrame frame = new JFrame(TITLE);

        // Create an instance of the applet
        BallsGame applet = new BallsGame();

        // Add the applet instance to the frame
        frame.add(applet, BorderLayout.CENTER);

        // Invoke applet's init method
        applet.init();
        applet.start();

        // Display the frame
        frame.pack();
        frame.setLocationRelativeTo(null); // Center the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
